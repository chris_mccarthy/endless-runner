﻿using System.Collections;
using System.Collections.Generic;
using EndlessRunner.Gameplay;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace EndlessRunner.UI
{
    [RequireComponent(typeof(Text))]
    public class ScoreText : MonoBehaviour
    {
        [Inject] IScoreController scoreController;

        private void Start()
        {
            var text = GetComponent<Text>();

            scoreController.CurrentScore
                .TakeUntilDestroy(this)
                .Subscribe(score => text.text = score.ToString());
        }
    }
}
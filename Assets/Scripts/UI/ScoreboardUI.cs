﻿using System.Collections;
using System.Collections.Generic;
using EndlessRunner.Gameplay;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using UniRx.Triggers;
using System.Text;
using UnityEngine.SceneManagement;
using System.Linq;

namespace EndlessRunner.UI
{
    public class ScoreboardUI : MonoBehaviour
    {

        [SerializeField] Text scoreText;
        [SerializeField] InputField intialText;
        [SerializeField] Button enterButton;
        [SerializeField] GameObject submitRoot;
        [SerializeField] GameObject scoreboardRoot;
        [SerializeField] Text scoreboardText;
        [SerializeField] Button scoreboardDoneButton;

        [Inject] IScoreController scoreController;
        [Inject] IScoreRepository scoreRepository;
        [Inject] IGameEventsController gameEventsController;

        private void Start()
        {
            // Activate the submission UI when the game is over
            gameEventsController.OnGameOver
                .TakeUntilDestroy(this)
                .Subscribe(_ =>
                {
                    scoreText.text = "You got: " + scoreController.CurrentScore.Value;
                    enterButton.interactable = false;
                    intialText.text = string.Empty;

                    submitRoot.SetActive(true);
                });

            // Activate the enter button when intials entered are valid
            intialText.OnValueChangedAsObservable()
                .TakeUntilDestroy(this)
                .Select(text => text.Length == 3)
                .Subscribe(intialsEntered => enterButton.interactable = intialsEntered);


            // When the submit button is clicked, submit the score an deactivate the UI
            enterButton.OnClickAsObservable()
                .TakeUntilDestroy(this)
                .Subscribe(_ =>
                {
                    // Submit the score
                    scoreRepository.SubmitScore(new ScoreData
                    {
                        owner = intialText.text,
                        score = scoreController.CurrentScore.Value
                    });
                    submitRoot.SetActive(false);

                    // Make an ordered list of the top 5 scores
                    var dataList = scoreRepository.Data
                        .OrderBy(data => -data.score)
                        .ToList()
                        .GetRange(0, Mathf.Min(5, scoreRepository.Data.Count()));

                    var scoreboardDataText = new StringBuilder();
                    foreach (var t in dataList)
                    {
                        scoreboardDataText.Append(t.owner);
                        scoreboardDataText.Append(": ");
                        scoreboardDataText.Append(t.score);
                        scoreboardDataText.Append("\n");
                    }

                    // Apply this data to the text and activate the scoreboard
                    scoreboardText.text = scoreboardDataText.ToString();
                    scoreboardRoot.SetActive(true);
                });

            // When the scoreboard done button is clicked, reload the scene
            scoreboardDoneButton.OnClickAsObservable()
                .TakeUntilDestroy(this)
                .Subscribe(_ => SceneManager.LoadScene(SceneManager.GetActiveScene().name));// HACK: using scene loads to reload a level is a dangerous habit

            // Deactivate all roots on launch
            scoreboardRoot.SetActive(false);
            submitRoot.SetActive(false);
        }
    }
}
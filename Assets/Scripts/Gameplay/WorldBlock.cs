﻿using UnityEngine;

namespace EndlessRunner.Gameplay
{
    public class WorldBlock : MonoBehaviour
    {
        [SerializeField] Transform startPin;
        [SerializeField] Transform endPin;

        public Vector3 StartPosition => startPin.position;
        public Vector3 EndPosition => endPin.position;

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(startPin.position, 1f);
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(endPin.position, 1f);
            Gizmos.color = Color.white;
        }
    }
}
﻿using UniRx;
using UnityEngine;
using Zenject;

namespace EndlessRunner.Gameplay
{
    public class DeathWallController : MonoBehaviour
    {
        [SerializeField] Transform rootTransform;
        [SerializeField] float forwardVelocity;
        [SerializeField] float maxDistance;

        [Inject] IInputController inputController;
        [Inject] IPlayer player;

        private void Start()
        {
            // Start the wall movement after first click
            inputController.OnJump()
                .Where(_ => player.IsReady)
                .First()
                .TakeUntilDestroy(this)
                .Subscribe(_ => Init());
        }

        void Init()
        {
            var velocity = forwardVelocity;

            // If the wall collides with the player, reload the level
            Observable.EveryUpdate()
                .Select(_ => player.Position.x < rootTransform.position.x)
                .DistinctUntilChanged()
                .First(isDead => isDead)
                .TakeUntilDestroy(this)
                .Subscribe(_ => player.Kill());

            // When the death wall is too close, slow it down so the player can gain distance from it
            Observable.EveryUpdate()
                .TakeUntilDestroy(this)
                .Subscribe(_ => velocity = (Mathf.Abs(rootTransform.position.x - player.Position.x) < maxDistance) ? forwardVelocity * .5f : forwardVelocity);

            // Move the wall forward every update
            Observable.EveryUpdate()
                .TakeUntilDestroy(this)
                .Subscribe(_ => rootTransform.position += Vector3.right * velocity * Time.deltaTime);

            // Have the wall follow along the y axis
            Observable.EveryUpdate()
                .TakeUntilDestroy(this)
                .Subscribe(_ => rootTransform.position = new Vector3(rootTransform.position.x, player.Position.y));
        }

        private void OnDrawGizmos()
        {
            Gizmos.DrawWireCube(rootTransform.position, Vector3.up * 20);
        }
    }
}
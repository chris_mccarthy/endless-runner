﻿using UniRx;
using UniRx.Triggers;
using UnityEngine;
using Zenject;

namespace EndlessRunner.Gameplay
{
    public class CoinController : MonoBehaviour
    {
        [SerializeField] Collider collider;
        [SerializeField] GameObject visualRoot;
        [SerializeField] Transform effectPin;
        [SerializeField] GameObject collectionEffect;
        [SerializeField] float spinSpeed = 360;
        [SerializeField] int coinValue = 10;

        [Inject] IScoreController scoreController;
        [Inject] IPlayer player;
        [Inject] DiContainer container;

        private void Start()
        {
            // Spin the coin
            Observable.EveryUpdate()
                .TakeUntilDestroy(this)
                .Select(_ => Time.time * spinSpeed)
                .Subscribe(rotation => visualRoot.transform.localEulerAngles = new Vector3(0, rotation, 0));

            // Explode and hide the coin and add the score
            collider.OnTriggerEnterAsObservable()
                .TakeUntilDestroy(this)
                .Where(other => other == player.PlayerCollider)
                .Subscribe(_ =>
                {
                    var effect = container.InstantiatePrefab(collectionEffect);
                    effect.transform.position = effectPin.position;
                    effect.transform.parent = effectPin;
                    visualRoot.SetActive(false);
                    collider.enabled = false;

                    scoreController.AddScore(coinValue);
                });
        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace EndlessRunner
{
    public interface IGameEventsController
    {
        Subject<Unit> OnGameOver { get; }
    }

    public class GameEventsController : IGameEventsController
    {
        public Subject<Unit> OnGameOver { get; } = new Subject<Unit>();
    }
}

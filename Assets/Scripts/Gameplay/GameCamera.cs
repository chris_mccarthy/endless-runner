﻿using UnityEngine;

namespace EndlessRunner.Gameplay
{
    public interface IGameCamera
    {
        Rect CameraRect { get; }
    }

    public class GameCamera : MonoBehaviour, IGameCamera
    {
        [SerializeField] Camera gameCamera;

        public Rect CameraRect
        {
            get
            {
                var plane = new Plane(gameCamera.transform.forward, 0);
                var minRay = gameCamera.ViewportPointToRay(Vector3.zero);
                var maxRay = gameCamera.ViewportPointToRay(Vector3.one);

                plane.Raycast(minRay, out float minFloat);
                plane.Raycast(maxRay, out float maxFloat);

                var min = minRay.GetPoint(minFloat);
                var max = maxRay.GetPoint(maxFloat);

                return Rect.MinMaxRect(
                        min.x,
                        min.y,
                        max.x,
                        max.y
                    );
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireCube(CameraRect.center, CameraRect.size);
            Gizmos.color = Color.white;
        }
    }
}
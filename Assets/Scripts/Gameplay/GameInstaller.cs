﻿using UnityEngine;
using Zenject;

namespace EndlessRunner.Gameplay
{
    public class GameInstaller : MonoInstaller
    {
        [SerializeField] PlayerController playerController;
        [SerializeField] InputController inputController;
        [SerializeField] GameCamera gameCamera;

        public override void InstallBindings()
        {
            Container.Bind<IPlayer>()
                .To<PlayerController>()
                .FromInstance(playerController)
                .AsSingle();

            Container.Bind<IInputController>()
                .To<InputController>()
                .FromInstance(inputController)
                .AsSingle();

            Container.Bind<IGameCamera>()
                .To<GameCamera>()
                .FromInstance(gameCamera)
                .AsSingle();

            Container.BindInterfacesTo<ScoreController>()
                .AsSingle()
                .NonLazy();

            Container.BindInterfacesTo<GameEventsController>()
                .AsSingle()
                .NonLazy();

        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using Zenject;

namespace EndlessRunner.Gameplay
{
    [RequireComponent(typeof(Rigidbody))]
    public class PhysicsBlock : MonoBehaviour
    {
        [SerializeField] float selfDestructDistance = 20f;
        [Inject] IPlayer player;

        private void Start()
        {
            var blockRigidbody = GetComponent<Rigidbody>();

            Observable.EveryFixedUpdate()
                .First(_ => !blockRigidbody.IsSleeping())
                .TakeUntilDestroy(this)
                .Subscribe(_ =>
                {
                    transform.parent = null;

                    Observable.EveryUpdate()
                        .First(__ => Vector3.Distance(player.Position, blockRigidbody.position) > selfDestructDistance)
                        .TakeUntilDestroy(this)
                        .Subscribe(__ => Destroy(blockRigidbody.gameObject));
                });
        }
    }
}
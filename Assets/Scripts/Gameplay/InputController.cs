﻿using System;
using System.Collections;
using System.Collections.Generic;
using EndlessRunner.Utils;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;

namespace EndlessRunner.Gameplay
{
    public interface IInputController
    {
        IObservable<Unit> OnJump();
    }

    public class InputController : MonoBehaviour, IInputController
    {
        [SerializeField] EventTrigger touchTrigger;

        public IObservable<Unit> OnJump()
        {
            return touchTrigger.ObserveTrigger(EventTriggerType.PointerDown)
                .Select(_ => Unit.Default);
        }
    }
}
﻿using System;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace EndlessRunner.Gameplay
{
    public interface IPlayer
    {
        /// <summary>
        /// The position of the player
        /// </summary>
        /// <value>The position of the player</value>
        Vector3 Position { get; }
        Collider PlayerCollider { get; }
        bool IsReady { get; }
        void Kill();
    }

    public class PlayerController : MonoBehaviour, IPlayer
    {
        [SerializeField] Rigidbody playerRigidbody;
        [SerializeField] Collider playerCollider;
        [SerializeField] Animator playerAnimator;
        [SerializeField] Transform animationRoot;
        [SerializeField] AudioSource audioSource;
        [SerializeField] AudioClip jumpSound;
        [SerializeField] AudioClip deathSound;
        [SerializeField] float jumpForce;
        [SerializeField] float forwardAcceleration;
        [SerializeField] float forwardMaxVelocity;
        [SerializeField] float distanceScoreMultiplier;

        [Inject] IInputController inputController;
        [Inject] IScoreController scoreController;
        [Inject] IGameEventsController gameEventsController;

        CompositeDisposable disposables = new CompositeDisposable();

        public Vector3 Position => playerRigidbody.position;
        public Collider PlayerCollider => playerCollider;
        public bool IsReady => !playerAnimator.GetCurrentAnimatorStateInfo(0).IsName("Respawn");

        void Start()
        {
            // Dont trigger the player movement until first click
            inputController.OnJump()
                .Where(_ => IsReady)
                .First()
                .TakeUntilDestroy(this)
                .Subscribe(_ => Init());
        }

        void Init()
        {
            // Use this var to determine if the rigidbody is grounded
            var isGrounded = true;

            // If the player's vertical speed is less than or equal to minimal threshold, the player is at rest
            Observable.EveryFixedUpdate()
                .Select(_ => Mathf.Abs(playerRigidbody.velocity.y) <= float.Epsilon)
                .DistinctUntilChanged()
                .Subscribe(atRest => isGrounded = atRest)
                .AddTo(disposables);

            // Jump on every click
            inputController.OnJump()
                .Where(_ => isGrounded) // if the rigidbody is at rest, move forward with the jump event
                .Subscribe(_ =>
                {
                    playerRigidbody.AddForce(Vector3.up * jumpForce, ForceMode.VelocityChange);

                    audioSource.clip = jumpSound;
                    audioSource.loop = false;
                    audioSource.Play();
                })
                .AddTo(disposables);

            // Add constant force to move the player forward until they reach the speed necessary
            Observable.EveryFixedUpdate()
                .Subscribe(_ =>
                {
                    if (playerRigidbody.velocity.x < forwardMaxVelocity)
                        playerRigidbody.AddForce(Vector3.right * forwardAcceleration, ForceMode.Acceleration);
                    else
                        playerRigidbody.velocity = new Vector3(forwardMaxVelocity, playerRigidbody.velocity.y);

                })
                .AddTo(disposables);

            // These are used to determine how much score to add
            var startLocation = playerRigidbody.position.x;
            var lastScore = 0;
            var appliedVerticalSpeed = 0f;

            // Add the score from the new distance
            Observable.EveryUpdate()
                .Select(_ => Mathf.FloorToInt((playerRigidbody.position.x - startLocation) * distanceScoreMultiplier))
                .Where(newScore => newScore > lastScore)
                .Subscribe(newScore =>
                {
                    var addedScore = newScore - lastScore;
                    scoreController.AddScore(addedScore);
                    lastScore = newScore;
                })
                .AddTo(disposables);

            // Manage the animations
            Observable.EveryLateUpdate()
                .Subscribe(_ =>
                {
                    // HACK: this is a work around for an issue with the root moving in the landing animation
                    animationRoot.localPosition = new Vector3(animationRoot.localPosition.x, animationRoot.localPosition.y, 0);
                    playerAnimator.SetBool("Grounded", isGrounded);

                    playerAnimator.SetFloat("ForwardSpeed", playerRigidbody.velocity.x);

                    // Added a little lerping to make the jump animations look smoother
                    // TODO: turn this into a constant
                    appliedVerticalSpeed = Mathf.Lerp(appliedVerticalSpeed, playerRigidbody.velocity.y, .03f);
                    playerAnimator.SetFloat("VerticalSpeed", appliedVerticalSpeed);
                    playerAnimator.SetFloat("AirborneVerticalSpeed", appliedVerticalSpeed);
                })
                .AddTo(disposables);

            //Kill the player if theyve been falling for too long
            IDisposable deathTimerDisposable = null;

            Observable.EveryUpdate()
                .Select(_ => isGrounded)
                .DistinctUntilChanged()
                .Subscribe(grounded =>
                {
                    if (grounded)
                        deathTimerDisposable?.Dispose();
                    else
                        deathTimerDisposable = Observable.Timer(TimeSpan.FromSeconds(3f))
                            .Subscribe(_ => Kill())
                            .AddTo(disposables);
                })
                .AddTo(disposables);
        }

        public void Kill()
        {
            disposables.Clear();
            playerAnimator.SetTrigger("Death");

            audioSource.clip = deathSound;
            audioSource.loop = false;
            audioSource.Play();

            Observable.Timer(TimeSpan.FromSeconds(2f))
                .First()
                .Subscribe(_ => gameEventsController.OnGameOver.OnNext(Unit.Default));
        }

        private void OnDestroy()
        {
            disposables.Dispose();
        }
    }
}
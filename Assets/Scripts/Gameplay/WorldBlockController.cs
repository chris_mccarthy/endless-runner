﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using Zenject;

namespace EndlessRunner.Gameplay
{
    public class WorldBlockController : MonoBehaviour
    {
        [SerializeField] Transform startPositionPin;
        [SerializeField] WorldBlock[] worldBlocks;

        [Inject] DiContainer container;
        [Inject] IGameCamera gameCamera;

        private void Start()
        {
            var spawnedBlocks = new List<WorldBlock>();
            var currentEndPosition = startPositionPin.position;

            Observable.EveryUpdate()
                .TakeUntilDestroy(this)
                .Subscribe(_ =>
                {
                    // Create new blocks
                    for (int a = 0; a < 3 && currentEndPosition.x < gameCamera.CameraRect.max.x; a++)
                    {
                        // Choose a random block prefab
                        var randomPrefab = worldBlocks[Mathf.FloorToInt(Random.value * worldBlocks.Length)];
                        var instance = container.InstantiatePrefab(randomPrefab).GetComponent<WorldBlock>();
                        instance.transform.parent = transform;

                        // Apply the offset that needs to be added to link the blocks
                        instance.transform.position += currentEndPosition - instance.StartPosition;

                        // Set the current end position to the end of the last block made
                        currentEndPosition = instance.EndPosition;

                        // Add the spawned to the list of spawned 
                        spawnedBlocks.Add(instance);
                    }

                    // Flush all the old blocks
                    for (int a = 0; a < 3; a++)
                    {
                        var first = spawnedBlocks.FirstOrDefault();

                        if (first != null && first.EndPosition.x < gameCamera.CameraRect.min.x)
                        {
                            spawnedBlocks.Remove(first);
                            Destroy(first.gameObject);
                        }
                        else
                            break;
                    }
                });

        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.DrawWireSphere(startPositionPin.position, 1f);
        }
    }
}
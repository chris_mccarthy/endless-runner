﻿using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;

namespace EndlessRunner.Gameplay
{
    /// <summary>
    /// The data about a score on the scoreboard;
    /// </summary>
    [System.Serializable]
    public class ScoreData
    {
        public string owner;
        public int score;
    }

    /// <summary>
    /// Used to manage and monitor the current score of the game
    /// </summary>
    public interface IScoreController
    {
        IReadOnlyReactiveProperty<int> CurrentScore { get; }
        void AddScore(int scoreAdded);
    }

    /// <summary>
    /// For saving and retrieving past scores
    /// </summary>
    public interface IScoreRepository
    {
        void SubmitScore(ScoreData scoreData);
        IEnumerable<ScoreData> Data { get; }
    }

    public class ScoreController : IScoreController, IScoreRepository
    {
        // Used entirely for json serialization
        private class ScoreList
        {
            public List<ScoreData> data;
        }

        private const string PlayerPrefsKey = "Score";

        private ReactiveProperty<int> score = new ReactiveProperty<int>();

        public IReadOnlyReactiveProperty<int> CurrentScore => score;
        public IEnumerable<ScoreData> Data => JsonUtility.FromJson<ScoreList>(PlayerPrefs.GetString(PlayerPrefsKey))?.data ?? new List<ScoreData>();

        public void AddScore(int scoreAdded)
        {
            score.Value += scoreAdded;
        }

        public void SubmitScore(ScoreData scoreData)
        {
            var currentScore = Data.ToList();

            currentScore.Add(scoreData);

            // Save the new list of scores
            var json = JsonUtility.ToJson(new ScoreList
            {
                data = currentScore
            });
            PlayerPrefs.SetString(PlayerPrefsKey, json);
            PlayerPrefs.Save();
        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace EndlessRunner.Utils
{
    public static class UnirxUtils
    {
        /// <summary>
        /// Observes trigger events on a event trigger
        /// </summary>
        /// <returns>The observable to subscribe to</returns>
        /// <param name="triggerType">The pointer event being observed</param>
        public static IObservable<PointerEventData> ObserveTrigger(this EventTrigger trigger, EventTriggerType triggerType)
        {
            EventTrigger.Entry entry = new EventTrigger.Entry
            {
                eventID = triggerType
            };

            return Observable.FromEvent<PointerEventData>(
                action =>
                {
                    // Add the listener to the trigger
                    entry.callback.AddListener((data) => action((PointerEventData)data));
                    trigger.triggers.Add(entry);
                },
                action =>
                {
                    // immediately remove the callback
                    entry.callback = new EventTrigger.TriggerEvent();
                    // when it is safe in the event order, remove the listener
                    Observable.EveryEndOfFrame()
                        .First()
                        .Subscribe(_ => trigger.triggers.Remove(entry));
                });
        }
    }
}